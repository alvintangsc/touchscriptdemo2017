﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TouchScript.Gestures;

public class FlickHandler : MonoBehaviour
{
    // reference to the player we want this gesture handler to affect
    // remember to drag-drop the player object to this script in the Unity inspector
    public GameObject player;

    // reference to the player's rigidbody to affect jump
    Rigidbody2D playerRigidBody;

    // jump velocity - remember to set this in the Unity inspector
    public float jumpSpeedY = 300.0f;

	// Use this for initialization
	void Start ()
    {
        playerRigidBody = player.GetComponent<Rigidbody2D>();
	}

    private void OnEnable()
    {
        GetComponent<FlickGesture>().Flicked += flickHandler;
    }
    private void OnDisable()
    {
        GetComponent<FlickGesture>().Flicked -= flickHandler;
    }

    private void flickHandler(object sender, System.EventArgs e)
    {
        // call the function on the player's script to react
        playerRigidBody.AddForce(new Vector2(playerRigidBody.velocity.x, jumpSpeedY));
        Debug.Log("flick!");
    }

    // Update is called once per frame
    void Update ()
    {
		
	}
}
